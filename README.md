# How to publish to the Chrome Store

## Update package

Once a package has been uploaded/published, it can easily be updated through the developer dashboard.
There is no need to sign the package with a private key.

1. Make sure the private keys are available (see backup) (update: no longer required if working through dashboard)
2. ZIP the files in the development folder (including manifest and all required files)
2. Navigate to [Developer Dashboard](https://chrome.google.com/webstore/developer/dashboard)
3. Select the Drive Companion application
4. Update the application by uploading the new ZIP file
5. The application Draft should be updated with the new version
6. Publish when ready.
