// const COMPANION_NAME = chrome.runtime.getManifest().name;
// const COMPANION_ID = chrome.runtime.getManifest().key;
// const COMPANION_VERSION = chrome.runtime.getManifest().version;

// setupGoogleAnalytics();


// function setupGoogleAnalytics() {

//     if (!window.ga) {
//       (function(){
//         window.ga = function() {
//           (window.ga.q = window.ga.q || []).push(arguments);
//         }, window.ga.l = 1 * new Date();
//         var tag = 'script';
//         var a = document.createElement(tag);
//         var m = document.getElementsByTagName(tag)[0];
//         a.async = 1;
//         a.src = 'https://www.google-analytics.com/analytics.js';
//         m.parentNode.insertBefore(a, m);
//       })();

//       ga('create', 'UA-82062866-3', 'auto');
//       ga('set', 'checkProtocolTask', null);
//       ga('set', 'appName', COMPANION_NAME);
//       ga('set', 'appId', COMPANION_ID);
//       ga('set', 'appVersion', COMPANION_VERSION);
//     }
//   }

//   function sendPageView(page){
//     ga('set', 'screenName', page);
//     ga('send', 'screenview', page);
//   }

//   function sendEvent(category, action, label, value) {
//     ga('send', 'event', category, action, label, value)
//   }

//   function sendException(description, isFatal=false) {
//     ga('send', 'exception', {'exDescription': description, 'exFatal': isFatal});
//   }