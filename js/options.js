document.addEventListener('DOMContentLoaded', function () {
	var linkOptions = document.querySelectorAll(".linkoption_item");
	linkOptions.forEach(el => {
		restore_check_option(el);
		el.addEventListener("change", save_check_option);
	});

	var iconOptions = document.querySelectorAll(".toolbaricons_item");
	iconOptions.forEach(el => el.addEventListener("click", toolbar_icon_click));
});

function toolbar_icon_click(item) {
	// var iconPath = item.target.currentSrc
	// chrome.storage.local.set({
	// 	toolbaricon : item.target.currentSrc
	// }, function () {
	// 	console.log("setting saved toolbaricon value: " + item.target.currentSrc);
	// 	chrome.browserAction.setIcon({path: item.target.currentSrc});
	// });
}

function restore_check_option(item) {
	var key = item.id;
	var keyDefault = item.defaultChecked;
	chrome.storage.local.get({
		[key]: keyDefault
	}, function (setting) {
		console.log("restoring option " + key + " value: " + setting[key]);
		item.checked = setting[key];
		set_dependent_options(key);
	});
}

function save_check_option(item) {
	var key = item.target.id;
	chrome.storage.local.set({
		[key]: item.target.checked
	}, function () {
		console.log("setting saved " + key + " value: " + item.target.checked);
		set_dependent_options(key);
	});
}

function set_dependent_options(itemId) {
	var isEnabled = document.getElementById(itemId).checked;
	var things = document.querySelectorAll("[parent='" + itemId + "']");
	things.forEach(el => el.disabled = !isEnabled);
}
