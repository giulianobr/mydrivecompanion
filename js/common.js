var myHistory = [];

function showReleaseNote() {
	chrome.tabs.create({
		url: "http://drivecompanion.svelte.be/blog/release_2.2/"
	});
}

function processDriveRequest(details, key) {
	// if (key == "meet") {
	// 	if (popupEnabled) {
	// 		processCommonRequest(details.tabId, false);
	// 	}
	// } else {
	if (details.frameId == 0 && details.parentFrameId == -1) {
		chrome.storage.local.get({
			[key + '_popup']: true,
			[key + '_popup_maximize']: false
		}, function (setting) {
			var popupEnabled = setting[key + '_popup'];
			var maximize = setting[key + '_popup_maximize'];
			console.log("Processing request for " + key + " [popup: " + popupEnabled + " - maximize: " + maximize + "]");
			if (popupEnabled) {
				processCommonRequest(details.tabId, maximize);
				//sendPageView("Drive Companion");
				sendEvent("Application", "Open " + key, "[maximized: " + maximize + "]", 1)
			}
		});
	}
	// }
}

function processCommonRequest(tabId, maximize) {
	chrome.tabs.get(tabId, function (tab) {
		chrome.windows.get(tab.windowId, function (window) {
			if (window.type != "popup") {
				focusPreviousCompanionItem();
				console.log("create window");
				var windowstate = "normal";
				if (maximize) windowstate = "maximized";
				chrome.windows.create({
					tabId: tabId,
					type: "popup",
					focused: true,
					state: windowstate
				}, function (window) {
					console.log("adding id " + window.id + " to history");
					myHistory.push(window.id);
				});

			}
		});
	});
}

function focusPreviousCompanionItem() {
	if (myHistory.length > 0) {
		console.log("update window order");
		chrome.windows.update(myHistory[myHistory.length - 1], {
			focused: true
		});
	}
}
