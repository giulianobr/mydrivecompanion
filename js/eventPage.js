const KEY = {
	DRIVE: "drive",
	DOCS: "docs",
	SHEETS: "sheets",
	SLIDES: "slides",
	FORMS: "forms",
	DRAWINGS: "drawings",
	MEET: "meet"
}

const ICON = {
	DEFAULT_ICON: "/images/icon16.png"

}

// Set selected icon
// chrome.storage.local.get({
// 	['toolbaricon']: ICON.DEFAULT_ICON
// }, function (setting) {
// 	chrome.browserAction.setIcon({
// 		path: setting.toolbaricon
// 	});
// });

chrome.webNavigation.onCommitted.addListener(function (details) {
	processDriveRequest(details, KEY.MEET);
}, {
	url: [{
		hostEquals: 'meet.google.com',
		pathPrefix: "/"
	}]
});

chrome.webNavigation.onCommitted.addListener(function (details) {
	processDriveRequest(details, KEY.DRIVE);
}, {
	url: [{
		hostEquals: 'drive.google.com',
		pathPrefix: "/drive"
	}]
});

chrome.webNavigation.onCommitted.addListener(function (details) {
	processDriveRequest(details, KEY.DOCS);
}, {
	url: [{
		hostEquals: 'docs.google.com',
		pathPrefix: "/document"
	}]
});

chrome.webNavigation.onCommitted.addListener(function (details) {
	processDriveRequest(details, KEY.SHEETS);
}, {
	url: [{
		hostEquals: 'docs.google.com',
		pathPrefix: "/spreadsheets"
	}]
});

chrome.webNavigation.onCommitted.addListener(function (details) {
	processDriveRequest(details, KEY.SLIDES);
}, {
	url: [{
		hostEquals: 'docs.google.com',
		pathPrefix: "/presentation"
	}]
});

chrome.windows.onRemoved.addListener(function (windowId) {
	var index = myHistory.indexOf(windowId);
	if (index > -1) {
		myHistory.splice(index, 1);
	}
}, {
	windowTypes: ["popup"]
});

chrome.windows.onFocusChanged.addListener(function (windowId) {
	var index = myHistory.indexOf(windowId);
	if (index > -1) {
		myHistory.push(myHistory.splice(index, 1)[0]);
	}
}, {
	windowTypes: ["popup"]
});

// Check whether new version is installed
chrome.runtime.onInstalled.addListener(function (details) {
	var thisVersion = chrome.runtime.getManifest().version;
	// if(details.reason == "install"){
	//     sendEvent("Application", "First install", thisVersion);
	// } else if (details.reason == "update") {
	// 	sendEvent("Application", "Update", details.previousVersion + " -> " + thisVersion);
	// 	showReleaseNote();
	// }
});
