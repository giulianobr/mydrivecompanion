document.addEventListener('DOMContentLoaded', function () {
	var links = document.querySelectorAll('.btn-flat[url],.menu-item[url]');
	links.forEach(el => el.addEventListener('click', openLink))

	document.querySelector('#go-to-options').addEventListener('click', openOptions);
	document.querySelector('#add-current-tab').addEventListener('click', moveCurrentTabToWindow);
	var dropdown = document.querySelector(".dropdown-trigger");
	
	M.Dropdown.init(dropdown, { 
		coverTrigger: false, 
		constrainWidth: false
	});
});

//FUNCTIONS 
function openLink(event){
	var url = event.currentTarget.getAttribute("url");
	console.log(url);
	chrome.tabs.create({active: true, url: url});
}

function openOptions() {
	if (chrome.runtime.openOptionsPage) {
		// New way to open options pages, if supported (Chrome 42+).
		chrome.runtime.openOptionsPage();
	} else {
		// Reasonable fallback.
		window.open(chrome.runtime.getURL('options.html'));
	}
}

function moveCurrentTabToWindow() {
	chrome.tabs.query({active: true, currentWindow: true}, function (tab){
		chrome.windows.create({tabId: tab[0].id, type: "popup", focused: true});
	});
}
