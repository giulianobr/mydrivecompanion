chrome.contextMenus.create({
	title: "Copy page URL",
	contexts:["all"],
	onclick: copyPageUrl
});

chrome.contextMenus.create({
	title: "Move to tab",
	contexts:["all"],
	onclick: moveToTab
});

function moveToTab() {
	console.log("moving to tab");
	chrome.tabs.query({active: true, currentWindow: true}, function (tab){
		chrome.windows.create({tabId: tab[0].id, type: "normal", focused: true});
	});
}

function copyPageUrl(){
	chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
		var url = tabs[0].url;
		console.log("url is: " + url);
		copyTextToClipboard(url);
	});
	
}

function copyTextToClipboard(text) {
	var copyFrom = document.createElement("textarea");
	copyFrom.textContent = text;
	document.body.appendChild(copyFrom);
	copyFrom.select();
	document.execCommand('copy');
	copyFrom.blur();
	document.body.removeChild(copyFrom);
  }
